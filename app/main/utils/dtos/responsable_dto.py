from flask_restx import Namespace, fields


class ResponsableDto:
    api = Namespace("Responsable", description="Responsable related operations")
    responsable = api.model(
        "responsable",
        {
            "name": fields.String(required=True, description="responsable's name"),
            "surname": fields.String(required=True, description="responsable's surname"),
            "email": fields.String(required=True, description="responsable's email address"),
            "cellphone": fields.String(required=True, description="responsable's cellphone"),
            "empresa_id": fields.Integer(required=True, description="empresa's id"),
        },
    )

    responsableGet = api.model(
        "responsableGet",
        {
             "name": fields.String(required=True, description="responsable's name"),
            "surname": fields.String(required=True, description="responsable's surname"),
            "email": fields.String(required=True, description="responsable's email address"),
            "cellphone": fields.String(required=True, description="responsable's cellphone"),
            "user_id": fields.Integer(required=True, description="user's id"),
            "empresa_id": fields.Integer(required=True, description="empresa's id"),

        },
    )



