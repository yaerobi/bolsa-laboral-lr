import unittest
from . import BaseTestClass
from app.main.service.role_service import (
    save_new_role,
    get_all_roles,
)
from app.main.model.user_rol import Role


class TestUserServices(BaseTestClass):

    def test_save_new_rol(self):
        data = dict()
        data["name"] = "moderator"

        save_new_role(data)

        role_result = Role.query.filter_by(name="moderator").first()

        self.assertEqual("moderator", role_result.name)

    def test_get_all_roles(self):
        self.assertEqual([], get_all_roles())
        data = dict()

        data["name"] = "users1"

        save_new_role(data)

        data["name"] = "users2"

        save_new_role(data)

        data["name"] = "users3"

        save_new_role(data)

        all_roles = get_all_roles()

        self.assertEqual(3, len(all_roles))
        for i, rol in enumerate(all_roles):
            self.assertEqual(f"users{i+1}", rol.name)


if __name__ == "__main__":
    unittest.main()
