from flask import Flask

from app.main.extensions import db, jwt, flask_bcrypt, mail
from .config import config_by_name


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    configure_extensions(app)
    return app


def configure_extensions(app):
    """configure flask extensions
    """
    db.init_app(app)
    jwt.init_app(app)
    flask_bcrypt.init_app(app)
    mail.init_app(app)

