"""add validate-mail in user and roles

Revision ID: 85df694cbcdb
Revises: d2151e1a609d
Create Date: 2022-06-19 21:34:09.015519

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '85df694cbcdb'
down_revision = 'd2151e1a609d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('validated_mail', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'validated_mail')
    # ### end Alembic commands ###
