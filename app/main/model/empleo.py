from app.main import db


class Empleo(db.Model):
    __tablename__ = "empleo"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(255), nullable=False)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)

    def __init__(self, talent_id: int, empleo_desc: str):
        self.talent_id = talent_id
        self.description = empleo_desc
