from flask import request
from flask_restx import Resource, fields
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.main.service.empresa_service import (save_new_empresa,
                                              get_a_empresa,
                                              edit_empresa,
                                              get_all_empresas)
from app.main.utils.decorators.role import admin_required
from app.main.utils.dtos.empresa_dto import LlankayEmpresaDto
from http import HTTPStatus


api = LlankayEmpresaDto.api
_llankay_empresa = LlankayEmpresaDto.empresa


response_model = api.model(
    "response_inscription",
    {
        "status": fields.String,
        "message": fields.String,
    },
)


@api.route("/create_new")
class EmpresaNew(Resource):
    """Handles HTTP requests to URL: /llankay/v1/empresa/create_new."""

    @api.response(int(HTTPStatus.CREATED), "Empresa Successfully registered.")
    @api.response(int(HTTPStatus.CONFLICT), "Empresa already exists.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("Empresa registration")
    @api.expect(_llankay_empresa, validate=False)
    @jwt_required()  # TODO decorador responsable_required()
    def post(self):
        """Create a new Empresa"""
        empresa = request.json
        user = get_jwt_identity()
        return save_new_empresa(empresa, user)


@api.route("/get_empresa")
class EmpresaGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/empresa/get_empresa."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_empresa)
    @api.doc(security='Bearer')
    @jwt_required()  # TODO decorador responsable_required()
    def get(self):
        """Get self Empresa"""
        user = get_jwt_identity()
        return get_a_empresa(user)


@api.route("/edit_empresa")
class EmpresaUpdate(Resource):
    """Handles HTTP requests to URL: /llankay/v1/empresa/edit_empresa."""

    @api.response(int(HTTPStatus.ACCEPTED), "Empresa Successfully updated.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.expect(_llankay_empresa, validate=False)
    @jwt_required()  # TODO decorador responsable_required()
    def put(self):
        """Edit Empresa"""
        empresa = request.json
        user = get_jwt_identity()
        return edit_empresa(empresa, user)


@api.route("/get_all")
class EmpresaList(Resource):
    """Handles HTTP requests to URL: /llankay/v1/empresa/get_all."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.marshal_list_with(_llankay_empresa)
    @admin_required()
    def get(self):
        """List all registered empresas"""
        response = get_all_empresas()
        return [resp.simple_data() for resp in response]
