from app.main import db
from datetime import date


class InscriptionCourses(db.Model):
    __tablename__ = "inscriptions_courses"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)
    courses_id = db.Column(db.Integer, db.ForeignKey("courses.id"), nullable=True)
    date_inscription = db.Column(db.Date, nullable=True)
    is_accepted = db.Column(db.Boolean, nullable=True, default=False)

    def __init__(self, talent_id: int, courses_id: int):
        self.talent_id = talent_id
        self.courses_id = courses_id
        self.date_inscription = date.today()
