"""Unit tests for api.user_... API endpoint."""
from http import HTTPStatus
import unittest
import datetime
from run import app, db
from app.main.model.user import Users
from app.test.util import EMAIL, PASSWORD, register_user, administrator_user
from . import BaseTestClass
from flask import url_for
from app.main.service.user_service import (
    get_a_user,
)

SUCCESS = "successfully registered"
EMAIL_ALREADY_EXISTS = f"Email {EMAIL}, is already registered"


class TestUserController(BaseTestClass):

    def test_user_register(self):
        response = register_user(EMAIL, PASSWORD)
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["status"], "success")
        self.assertEqual(response.json["message"], SUCCESS)

    def test_user_register_email_already_registered(self):
        user = Users(
                password_hash="hola",
                register_on=datetime.datetime.utcnow(),
                last_pass_change=datetime.datetime.utcnow(),
                email=EMAIL,
            )
        db.session.add(user)
        db.session.commit()
        response = register_user(EMAIL, PASSWORD)
        self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
        self.assertEqual(response.json["message"], EMAIL_ALREADY_EXISTS)

    def test_userUpdate(self):

        data = dict()
        data["email"] = "eamanu@yaerobi"
        data["role_id"] = 1

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi",
        )
        db.session.add(user1)
        db.session.commit()
        token = administrator_user()
        with app.test_client() as c:
            response = c.put(
                url_for("api.user_update_role"),
                json={
                    'email': "eamanu@yaerobi", 'role_id': 1
                },
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            user = get_a_user("eamanu@yaerobi")
            self.assertEqual("eamanu@yaerobi", user.email)
            self.assertEqual(1, user.role_id)
            self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
            self.assertEqual(response.json["message"], "User Updated.")

    def test_user_get_controller(self):
        [register_user(f"eamanu{i}@yaerobi", "hola") for i in range(3)]
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.user_get"),

                json={
                    'email': "eamanu2@yaerobi",
                },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual("eamanu2@yaerobi", response.json["email"])
            self.assertEqual(HTTPStatus.OK, response.status_code)
            self.assertEqual(response.json["message"], "User Founded.")

        with app.test_client() as c:
            response = c.post(
                url_for("api.user_get"),

                json={
                    'email': "eamanu4@yaerobi",
                },

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def test_user_get_all_controller(self):
        [register_user(f"eamanu{i}@yaerobi", "hola") for i in range(3)]
        token = administrator_user()
        with app.test_client() as c:
            all_users = c.get(
                url_for("api.user_user_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, all_users.status_code)
            self.assertEqual(4, len(all_users.json))
            [self.assertEqual(f"eamanu{i}@yaerobi", user["email"])
             for i, user in enumerate(all_users.json) if i < 3]


if __name__ == "__main__":
    unittest.main()
