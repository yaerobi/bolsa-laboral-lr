from flask_restx import Api
from flask import Blueprint

from app.main.controller.user_controller import api as user_ns
from app.main.controller.auth_controller import api as auth_ns
from app.main.controller.talent_controller import api as inscription_ns
from app.main.controller.empresa_controller import api as empresa_ns
from app.main.controller.responsable_controller import api as responsable_ns
from app.main.controller.talent_inscription_controller import api as inscription_ns_cursos

# public
from app.main.controller.provinces_controller import api as provinces_ns


blueprint = Blueprint("api", __name__)

authorizations = {
    "Bearer": {"type": "apiKey",
               "in": "header",
               "name": "Authorization",
               'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"}}
api = Api(blueprint,
          title="Llankay",
          version="1.0",
          description="Llankay La Rioja",
          authorizations=authorizations,
          )

api.add_namespace(user_ns, path="/user")
api.add_namespace(auth_ns)
api.add_namespace(inscription_ns, path="/talent")
api.add_namespace(empresa_ns, path="/empresa")
api.add_namespace(responsable_ns, path="/responsable")
api.add_namespace(inscription_ns_cursos, path="/talent-courses")
api.add_namespace(provinces_ns, path="/public")
