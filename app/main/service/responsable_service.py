from app.main import db
from app.main.model.responsable import Responsable
from app.main.model.empresa import Empresa
from app.main.utils.db_utils import save_changes, get_all
from app.main.model.user import Users
from http import HTTPStatus
from flask import jsonify


def save_new_responsable(data, user_id):
    responsable = Responsable.query.filter_by(email=user_id).first()
    user = Users.query.filter_by(email=user_id).first()
    empresa = Empresa.query.filter_by(user_id=user.id).first()
    if not responsable:
        new_responsable = Responsable(
            name=data["name"],
            surname=data["surname"],
            email=user_id,
            cellphone=data["cellphone"],
            user_id=user.id,
            empresa_id=empresa.id,
        )
        save_changes(new_responsable)
        response = jsonify(
            status="success",
            message="Responsable Successfully registered.",
        )
        response.status_code = HTTPStatus.CREATED

        user.is_profile_complete = True
        save_changes(user)
        return response
    else:
        response = jsonify(
            status="fail",
            message=f"Responsable already exists. With {data['email']}.",
        )
        response.status_code = HTTPStatus.CONFLICT
        return response


def edit_responsable(data, user_id):
    responsable = Responsable.query.filter_by(email=user_id).first()
    responsable.name = data["name"]
    responsable.surname = data["surname"]
    responsable.email = user_id
    responsable.cellphone = data["cellphone"]

    db.session.commit()
    response = jsonify(
        status="success",
        message="Responsable Successfully updated.",
    )
    response.status_code = HTTPStatus.ACCEPTED
    return response


def get_a_responsable(email: str):
    return (
        db.session.query(Responsable)
        .filter(Responsable.email == email)
        .first()
    )


def get_all_responsable():
    return get_all(Responsable)
