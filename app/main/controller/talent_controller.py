from flask import request, jsonify
from flask_restx import Resource, fields
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.main.service.talent_service import (save_new_talent,
                                             get_a_talent,
                                             edit_talent,
                                             get_all_genre,
                                             get_all_residence,
                                             get_all_education_level,
                                             get_all_talent)
from app.main.utils.decorators.role import admin_required
from app.main.utils.dtos.talent_dto import LlankayTalentDto
from http import HTTPStatus


api = LlankayTalentDto.api
_llankay_talent = LlankayTalentDto.llankay_talent
_llankay_genre = LlankayTalentDto.genre
_llankay_residence = LlankayTalentDto.residence
_llankay_education_level = LlankayTalentDto.education_level


@api.route("/create_new")
class TalentNew(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/create_new."""

    @api.response(int(HTTPStatus.CREATED), "Talent Successfully registered.")
    @api.response(int(HTTPStatus.CONFLICT), "Talent already exists.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("Talent registration")
    @api.expect(_llankay_talent, validate=False)
    @jwt_required()
    def post(self):
        """Create a new Talent"""
        talent = request.json
        user = get_jwt_identity()
        return save_new_talent(talent, user)


@api.route("/get_talent")
class TalentGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/get_talent."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_talent)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get self Talent"""
        user = get_jwt_identity()
        return get_a_talent(user)


# TODO: Agregar DTO
@api.route("/edit_talent")
class TalentUpdate(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/edit_talent."""

    @api.response(int(HTTPStatus.ACCEPTED), "Talent Successfully updated.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.expect(_llankay_talent, validate=False)
    @jwt_required()
    def put(self):
        """Edit Talent"""
        talent = request.json
        user = get_jwt_identity()
        return edit_talent(talent, user)


@api.route("/get_genre_list")
class GenreGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/get_genre_list."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_genre)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get Genre List"""
        return get_all_genre()


@api.route("/get_residence_list")
class ResidenceGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/get_residence_list."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_residence)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get Residence List"""
        return get_all_residence()


@api.route("/get_educationlevel_list")
class EducationLevelGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/get_educationlevel_list."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_education_level)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get Education Level List"""
        return get_all_education_level()


@api.route("/get_all")
class TalentList(Resource):
    """Handles HTTP requests to URL: /llankay/v1/talent/get_all."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.marshal_list_with(_llankay_talent)
    @admin_required()
    def get(self):
        """List all registered talents"""
        response = get_all_talent()
        return [resp.simple_data() for resp in response]
