from app.main import db


SIMPLE_DATA_COLUMNS = ("name", "surname", "email", "dni", "cellphone")


class Talent(db.Model):
    __tablename__ = "talent"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    surname = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    dni = db.Column(db.String(10), unique=True, nullable=False)
    cellphone = db.Column(db.String(20), nullable=True)
    linkedin = db.Column(db.String(255), nullable=True)
    other_webpage = db.Column(db.String(255), nullable=True)
    bio = db.Column(db.String(255), nullable=True)
    github = db.Column(db.String(255), nullable=True)
    gitlab = db.Column(db.String(255), nullable=True)
    birthday = db.Column(db.Date, nullable=False)
    address = db.Column(db.String, nullable=False)
    education_level_id = db.Column(
        db.Integer, db.ForeignKey("education_level.id"), nullable=True
    )
    genre_id = db.Column(db.Integer, db.ForeignKey("genre.id"), nullable=True)
    larioja_depto_id = db.Column(
        db.Integer, db.ForeignKey("larioja_depto.id"), nullable=True
    )

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=True)
    empleo = db.relationship('Empleo', backref='talent')
    education = db.relationship('Education', backref='talent')
    # country_city_id = db.Column(
    #     db.Integer, db.ForeignKey("countries_cities.id"), nullable=False
    # )

    def simple_data(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name in SIMPLE_DATA_COLUMNS}

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
