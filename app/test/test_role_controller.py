"""Unit tests for api.user_role... API endpoint."""
from http import HTTPStatus
import unittest
from run import app, db
from app.main.model.user_rol import Role
from app.test.util import administrator_user, register_role
from . import BaseTestClass
from flask import url_for


class TestUserRolecontroller(BaseTestClass):

    def test_role_get_all_controller(self):
        token = administrator_user()
        [register_role(f"admin{i}", token.json['access_token']) for i in range(3)]
        with app.test_client() as c:
            all_roles = c.get(
                url_for("api.user_role_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            print(all_roles.json)
            self.assertEqual(HTTPStatus.OK, all_roles.status_code)
            self.assertEqual(4, len(all_roles.json))
            [self.assertEqual(f"admin{i}", roles["name"])
             for i, roles in enumerate(all_roles.json) if (i < 3 and any(map(str.isdigit, roles)))]

    def test_role_new_controller(self):
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.user_role_new"),
                json={
                    'name': "client",
                },
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["status"], "success")
            self.assertEqual(response.json["message"], "Role successfully registered")

    def test_user_register_email_already_registered(self):
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.user_role_new"),
                json={
                    'name': "admin",
                },
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
            self.assertEqual(response.json["status"], "fail")
            self.assertEqual(response.json["message"], "Role admin, is already registered")


if __name__ == "__main__":
    unittest.main()
