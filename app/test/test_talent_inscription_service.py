import unittest
import datetime

from run import db
from app.main.service.user_service import (
    get_all_users,
)
from app.main.service.talent_inscription_service import (
    save_new_talent,
    edit_talent,
    get_all_talent,
    get_a_talent,
)
from app.main.model.user import Users
from app.main.model.talent import Talent
from . import BaseTestClass


class TestTalentInscriptionServices(BaseTestClass):

    def test_save_new_talent(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime.utcnow()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = 1
        data["education_level"] = 1
        data["empleo"] = 1
        data["motivation"] = 1
        data["knowledge"] = [{"knowledge": 1, "level": 1}]
        data["course"] = [{"course": 1}]
        save_new_talent(data, "eamanu@yaerobi.com")

        talent_result = Talent.query.filter_by(email="eamanu@yaerobi.com").first()

        self.assertEqual("eamanu@yaerobi.com", talent_result.email)

    def test_get_all_talent(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="ricardo@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )
        talento1 = Talent(
            user_id=1,
            name="Ricardo",
            surname="Andino",
            email="ricardo@yaerobi",
            dni="1",
            cellphone="1",
            linkedin="linkedin",
            other_webpage="other_webpage",
            bio="bio",
            github="github",
            birthday=datetime.datetime.utcnow(),
            address="Fatima calle 1"
        )

        talento2 = Talent(
            user_id=2,
            name="Emmanuel",
            surname="Arias",
            email="eamanu2@yaerobi",
            dni="2",
            cellphone="2",
            linkedin="linkedin2",
            other_webpage="other_webpage2",
            bio="bio2",
            github="github2",
            birthday=datetime.datetime.utcnow(),
            address="Fatima calle 2"

        )

        db.session.add(user1)
        db.session.add(user2)

        db.session.add(talento1)
        db.session.add(talento2)

        db.session.commit()

        all_candidate = get_all_talent()

        self.assertEqual(2, len(all_candidate))
        for talent in all_candidate:
            user = Users.query.filter_by(email=talent.email).first()
            self.assertEqual(talent.email, user.email)
            self.assertIn(f"bio", talent.bio)
            self.assertIn(f"linkedin", talent.linkedin)

    def test_get_a_talent(self):
        self.assertEqual([], get_all_users())

        talento1 = Talent(
            name="Ricardo",
            surname="Andino",
            email="ricardo@yaerobi",
            dni="1",
            cellphone="1",
            linkedin="linkedin",
            other_webpage="other_webpage",
            bio="bio",
            github="github",
            birthday=datetime.datetime.utcnow(),
            address="Fatima calle 1"
        )

        talento2 = Talent(
            name="Emmanuel",
            surname="Arias",
            email="eamanu@yaerobi",
            dni="2",
            cellphone="2",
            linkedin="linkedin2",
            other_webpage="other_webpage2",
            bio="bio2",
            github="github2",
            birthday=datetime.datetime.utcnow(),
            address="Fatima calle 2"

        )

        db.session.add(talento1)
        db.session.add(talento2)

        db.session.commit()

        talent = get_a_talent("eamanu@yaerobi")

        self.assertEqual("Emmanuel", talent.name)
        self.assertEqual("Arias", talent.surname)
        self.assertEqual("eamanu@yaerobi", talent.email)
        self.assertEqual("linkedin2", talent.linkedin)
        self.assertEqual("bio2", talent.bio)

        talent = get_a_talent("ricardo@yaerobi")

        self.assertEqual("Ricardo", talent.name)
        self.assertEqual("Andino", talent.surname)
        self.assertEqual("ricardo@yaerobi", talent.email)
        self.assertEqual("linkedin", talent.linkedin)
        self.assertEqual("bio", talent.bio)

    def test_response_talent_update(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "1"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime.utcnow()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = 1
        data["education_level"] = 1
        data["empleo"] = 1
        data["motivation"] = 1
        data["knowledge"] = [{"knowledge": 1, "level": 1}]
        data["course"] = [{"course": 1}]

        response, code_status = save_new_talent(data, "eamanu@yaerobi.com")

        response_object = {
            "status": "success",
            "message": "Talent Successfully registered.",
        }
        self.assertEqual(response_object, response)
        self.assertEqual(201, code_status)

        data = dict()
        data["name"] = "Rodrigo"
        data["surname"] = "Rojas"
        data["email"] = "eamanu@yaerobi.com"
        data["dni"] = "2"
        data["cellphone"] = "2"
        data["linkedin"] = "linkedin2"
        data["other_webpage"] = "other_webpage2"
        data["bio"] = "bio2"
        data["github"] = "github2"
        data["gitlab"] = "gitlab2"
        data["birthday"] = datetime.datetime.utcnow()
        data["address"] = "Fatima calle 2"
        data["residence"] = 1
        data["genre"] = 1
        data["education"] = 1
        data["education_level"] = 1
        data["empleo"] = 1
        data["motivation"] = 1
        data["knowledge"] = [{"knowledge": 1, "level": 1}]
        data["course"] = [{"course": 1}]

        response, code_status = edit_talent(data, "eamanu@yaerobi.com")

        response_object = {"status": "success", "message": "Talent Updated."}

        self.assertEqual(response_object, response)
        self.assertEqual(201, code_status)


if __name__ == "__main__":
    unittest.main()
