from app.main import db


class TalentEmpleo(db.Model):
    __tablename__ = "talent_empleo"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)
    empleo_id = db.Column(db.Integer, db.ForeignKey("empleo.id"), nullable=True)

    def __init__(self, talent_id: int, empleo_id: int):
        self.talent_id = talent_id
        self.empleo_id = empleo_id
