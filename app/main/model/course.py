from app.main import db
from datetime import datetime
from typing import Optional


class Courses(db.Model):
    __tablename__ = "courses"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    is_from_gob_larioja = db.Column(db.Boolean, nullable=False, default=False)
    is_open = db.Column(db.Boolean, nullable=False, default=False)
    start = db.Column(db.Date, nullable=True)
    end = db.Column(db.Date, nullable=True)

    def __init__(
        self,
        name: str,
        description: str,
        is_from_gob_larioja: bool = False,
        is_open: bool = False,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
    ):
        self.name = name
        self.description = description
        self.is_from_gob_larioja = is_from_gob_larioja
        self.is_open = is_open
        self.start = start
        self.end = end
