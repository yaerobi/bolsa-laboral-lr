[![codecov](https://codecov.io/gh/Yaerobi/bolsa-laboral-lr/branch/main/graph/badge.svg?token=XHDLECG321)](https://codecov.io/gh/Yaerobi/bolsa-laboral-lr)


# bolsa-laboral-lr
Bolsa de Trabajo de La Rioja


# Base de datos

La base de datos que utiliza el sistema para desarrollo y producción es Postgres.
Para ello antes de levantar el sistema debemos tener instalado y funcionando Postgres.
Lo podemos de hacer de dos maneras, Instalando el sistema de manera local (no 
recomendado)  para ello ver `https://www.postgresql.org/download/`.

La otra manera (recomendad) es utilizar Docker. Para ello se debe [instalar Docker](https://docs.docker.com/engine/install/).
Una vez instalado ejecutar el siguiente comando:

```bash
docker run -d  --name llankay_db -p 5432:5432 -e POSTGRES_PASSWORD=llankaytest -e POSTGRES_DB=llankay -v <PONER_PATH_LOCAL>:/var/lib/postgresql/data postgres:11
```

Ejecutando `docker -ps` se puede observar si el contenedor for creado correctamente:

```bash
docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED         STATUS         PORTS                                       NAMES
6c51cf9fd7ca   postgres:11   "docker-entrypoint.s…"   8 minutes ago   Up 8 minutes   0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   llankay_db
```

Para acceder a la base de datos se debe realizar los siguiente, primer acceder
al contenedor:

```bash
docker exec -it llankay_db bash
root@6c51cf9fd7ca:/# 
```
Luego cambiar de usuario a postgres y acceder a la base de datos usando psql:

```bash
root@6c51cf9fd7ca:/# su postgres
postgres@6c51cf9fd7ca:/$ psql llankay
psql (11.13 (Debian 11.13-1.pgdg90+1))
Type "help" for help.

llankay=# \z
                            Access privileges
 Schema | Name | Type | Access privileges | Column privileges | Policies 
--------+------+------+-------------------+-------------------+----------
(0 rows)

llankay=# 
```

## URL de connección a la Base de Datos
La aplicación para conectarse a la base de datos, utiliza la variable de entorno
DATABASE_URL. Por lo que antes de levantar la aplicación se debe tener cuidado, de
tenerla asignada con los valores con correctos, ya sea de la base de datos local o del
contenedor creado. La cadena de conección tiene la siguiente forma:

```postgresql://<username>:<password>@<server>:5432/<db_name>```

Por lo que, para conectarse desde un linux se debería setear los siguiente:

```bash
export DATABASE_URL=postgresql://postgres:llankaytest@localhost:5432/llankay
```
 
# Levantar el sistema de backend

Para levantar el sistema de backend debemos tener instalado Python 3.9+. Luego
ejecutar los siguientes pasos:

  1. Crear virtual environment

  ```bash
  python3 -m venv venv
  ````

  2. Entrar al venv

  ```bash
  source venv/bin/activate
  ```

  3. Instalar los requrimientos

  ```bash
  pip install -r requirements.txt
  ```

  4. Exportar nombre de la app

  ```bash
  export FLASK_APP=run.py
  ```
  
  5. Exportar el environment de la app

  ```bash
  export FLASK_ENV=dev
  ```

  6. Ejecutar migraciones

  ```bash
  flask db upgrade
  ```

  6. Ejecutar app

  ```bash
  flask run
  ```

Se accede al `127.0.0.1:5000` y en /llankay/v1/ se encuentra el swagger.

## Usuario administrador

Usuario con rol admin

```
  email: admin@admin
  password: admin
```
