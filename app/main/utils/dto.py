from flask_restx import Namespace, fields


class UserDto:
    api = Namespace("user", description="user related operations")
    userCreate = api.model(
        "userCreate",
        {
            "email": fields.String(required=True, description="user email address"),
            "password": fields.String(required=True, description="user password"),
        },
    )
    userGet = api.model(
        "userGet",
        {
            "email": fields.String(required=True, description="user email address"),
        },
    )
    userUpdate = api.model(
        "userUpdate",
        {
            "email": fields.String(required=True, description="user email address"),
            "password": fields.String(required=False, description="user password"),
            "role_id": fields.Integer(required=False, description="user role"),
        },
    )
    Add_role = api.model(
        "Add_role",
        {
            "id": fields.Integer(required=False, description="role"),
            "name": fields.String(required=True, description="role"),

        },
    )


class AuthDto:
    api = Namespace("auth", description="authentication related operations")
    user_auth = api.model(
        "auth_details",
        {
            "email": fields.String(required=True, description="The user id"),
            "password": fields.String(required=True, description="The user password"),
        },
    )
    user_logout = api.model(
        "logout_details",
        {"email": fields.String(required=True, description="The user id")},
    )
