from http import HTTPStatus

from flask import jsonify
from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restx import Resource
from werkzeug.datastructures import FileStorage

from app.main.service.recover_password import send_recovery_password, recovery_password
from app.main.service.role_service import save_new_role, get_all_roles
from app.main.service.user_service import (
    save_new_user,
    get_all_users,
    get_a_user,
    edit_role_user_administrator,
    upload_files,
    get_file,
    is_profile_user_complete,
)
from app.main.utils.decorators.role import admin_required
from app.main.utils.dto import UserDto
from app.main.utils.entities import Roles

api = UserDto.api
_userCreate = UserDto.userCreate
_userGet = UserDto.userGet
_userUpdate = UserDto.userUpdate
_role = UserDto.Add_role

upload_parser = api.parser()
upload_parser.add_argument('file',
                           location='files',
                           type=FileStorage)


@api.route("/get_all")
class UserList(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/get_all."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.marshal_list_with(_userUpdate)
    @admin_required()
    def get(self):
        """List all registered users"""
        response = get_all_users()
        return response


@api.route("/create_new")
class UserRegister(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/create_new."""

    @api.response(int(HTTPStatus.CREATED), "New user was successfully created.")
    @api.response(int(HTTPStatus.CONFLICT), "Email address is already registered.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("create a new user")
    @api.expect(_userCreate, validate=True)
    def post(self):
        """Creates a new User"""
        data = request.json
        return save_new_user(data=data)


@api.route("/responsable/create_new")
class UserRegister(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/responsable/create_new."""

    @api.response(int(HTTPStatus.CREATED), "New responsable was successfully created.")
    @api.response(int(HTTPStatus.CONFLICT), "Email address is already registered.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("create a new responsable user")
    @api.expect(_userCreate, validate=True)
    def post(self):
        """Creates a new Responsable User"""
        data = request.json
        return save_new_user(data=data, role=Roles.BUSINESS_RESPONSABLE)


@api.route("/update_role", endpoint="user_update_role")
class UserUpdate(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/update_role."""

    @api.response(int(HTTPStatus.ACCEPTED), "User Updated.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("Update user role")
    @api.expect(_userUpdate, validate=True)
    @admin_required()
    def put(self):
        """Update a user by admin role, only update password and role by sending email as indentifier"""
        data = request.json
        return edit_role_user_administrator(data)


@api.route("/user_get", endpoint="user_get")
@api.response(int(HTTPStatus.NOT_FOUND), "User not found.")
class User(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/<email>."""

    @api.response(int(HTTPStatus.OK), "User Founded.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("get a user")
    @api.expect(_userGet, validate=True)
    @jwt_required()
    def post(self):
        """get a user given its identifier(email)"""
        data = request.json
        user = get_a_user(data["email"])
        if not user:
            api.abort(404)
        else:
            return jsonify(email=user.email,
                           status="success",
                           message="User Founded.",
                           )


@api.route("/role/get_all")
class RoleList(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/role/get_all."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.marshal_list_with(_role)
    @jwt_required()
    @admin_required()
    def get(self):
        """List all registered roles"""
        return get_all_roles()


@api.route("/role/create_new")
class RoleNew(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/role/create_new."""

    @api.response(int(HTTPStatus.CREATED), "New role was successfully created.")
    @api.response(int(HTTPStatus.CONFLICT), "Role is already registered.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.expect(_role, validate=True)
    @admin_required()
    def post(self):
        """Creates a new Role"""
        data = request.json
        return save_new_role(data=data)


@api.route("/recover-password/<email>")
class RecoverPassword(Resource):
    """Recover Password. This endpoint is used when users forget the password and need to change it."""
    @api.doc(params={"email": "The user email to be clean"})
    def get(self, email: str):
        """Recovery Password"""
        msg, status_code = send_recovery_password(email)
        return {"msg": msg}, status_code


# TODO: add a new endpoint to change a known password
@api.route("/change-password/<token>")
class RecoverPassword(Resource):
    """Change Password. This endpoint is used to change the password when a user forget it."""
    @api.doc(params={"token": "The token provided by backend"})
    def post(self, token: str):
        """Change Password"""
        data = request.json
        password = data["password"]
        msg, status_code = recovery_password(token, password)
        return {"msg": msg}, status_code


@api.route("/upload_photo")
class Upload(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/upload_photo."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.expect(upload_parser)
    @jwt_required()
    def post(self):
        """Upload photo"""
        args = upload_parser.parse_args()
        file = args.get('file')
        user = get_jwt_identity()
        return upload_files(file, user)


@api.route("/get_photo")
class GetFile(Resource):
    """Handles HTTP requests to URL: /llankay/v1/user/get_photo."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get photo"""
        user = get_jwt_identity()
        return get_file(user)


@api.route("/profile/complete")
class IsProfileComplete(Resource):
    """check if the user complete its data"""

    @api.doc(security="Bearer")
    @jwt_required()
    def get(self):
        """Check if the User completed its data"""
        user = get_jwt_identity()
        return ({"status": "success"}, 200) if is_profile_user_complete(user) else ({"status": "fail"}, 401)
