from app.main import db


class Province(db.Model):
    __tablename__ = "provinces"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
