TRUNCATE TABLE genre RESTART IDENTITY CASCADE;
INSERT INTO genre (name) VALUES('femenino');
INSERT INTO genre (name) VALUES('masculino');
INSERT INTO genre (name) VALUES('no binarie');
INSERT INTO genre (name) VALUES('otro');
INSERT INTO genre (name) VALUES('prefiero no responder');
INSERT INTO genre (name) VALUES('trans');

TRUNCATE TABLE larioja_depto RESTART IDENTITY CASCADE;
INSERT INTO larioja_depto (name) VALUES('Ángel V. Peñaloza');
INSERT INTO larioja_depto (name) VALUES('Arauco');
INSERT INTO larioja_depto (name) VALUES('Capital');
INSERT INTO larioja_depto (name) VALUES('Castro Barros');
INSERT INTO larioja_depto (name) VALUES('Chamical');
INSERT INTO larioja_depto (name) VALUES('Chilecito');
INSERT INTO larioja_depto (name) VALUES('Coronel Felipe Varela');
INSERT INTO larioja_depto (name) VALUES('Famatina');
INSERT INTO larioja_depto (name) VALUES('Manuel Belgrano');
INSERT INTO larioja_depto (name) VALUES('Juan Facundo Quiroga');
INSERT INTO larioja_depto (name) VALUES('General Lamadrid');
INSERT INTO larioja_depto (name) VALUES('General Ocampo');
INSERT INTO larioja_depto (name) VALUES('General San Martín');
INSERT INTO larioja_depto (name) VALUES('Independencia');
INSERT INTO larioja_depto (name) VALUES('Rosario Vera Peñaloza');
INSERT INTO larioja_depto (name) VALUES('San Blas de los Sauces');
INSERT INTO larioja_depto (name) VALUES('Sanagasta');
INSERT INTO larioja_depto (name) VALUES('Vinchina');


TRUNCATE TABLE education RESTART IDENTITY CASCADE;
INSERT INTO education (name) VALUES('Ingeniería en Sistemas/Informática/Computación/Sistemas de Información');
INSERT INTO education (name) VALUES('Licenciatura en Sistemas/Análisis de Sistemas/Informática');
INSERT INTO education (name) VALUES('Ingeniería en Electrónica/Electromecánica/Telecomunicaciones/Mecatrónica/Bioingenería');
INSERT INTO education (name) VALUES('Ingeniería Civil/Industrial/Mecánico/Electricista/Química/Física');
INSERT INTO education (name) VALUES('Estudiante de carrera de grado Ingenierías o Licenciaturas de sistemas de información');
INSERT INTO education (name) VALUES('Técnico Terciario o Universitario en Programación/Software/Sistemas/Informática');
INSERT INTO education (name) VALUES('Estudiante de carrera de pregrado en tecnologías de la información');
INSERT INTO education (name) VALUES('Estudiante de carreras de grado universitario en tecnologías de la información');
INSERT INTO education (name) VALUES('Egresado de carrera universitaria de otras áreas (Sociales, Económicas, Humanas, Derecho)');
INSERT INTO education (name) VALUES('Secundario Común');
INSERT INTO education (name) VALUES('Secundario Técnico');
INSERT INTO education (name) VALUES('Sin estudios formales/Idóneo/Autodidacta');

TRUNCATE TABLE education_level RESTART IDENTITY CASCADE;
INSERT INTO education_level (name) VALUES('Secundario Completo');
INSERT INTO education_level (name) VALUES('Terciario Incompleto');
INSERT INTO education_level (name) VALUES('Terciario Completo');
INSERT INTO education_level (name) VALUES('Universitario Incompleto');
INSERT INTO education_level (name) VALUES('Universitario Completo');
INSERT INTO education_level (name) VALUES('Posgrado Incompleto');
INSERT INTO education_level (name) VALUES('Posgrado Completo');


TRUNCATE TABLE level_knowledge RESTART IDENTITY CASCADE;
INSERT INTO level_knowledge (name) VALUES('Avanzado');
INSERT INTO level_knowledge (name) VALUES('Intermedio');
INSERT INTO level_knowledge (name) VALUES('Básico');
INSERT INTO level_knowledge (name) VALUES('Ninguno');

TRUNCATE TABLE knowledge RESTART IDENTITY CASCADE;
INSERT INTO knowledge (description) VALUES('Desarrollador Full Stack');
INSERT INTO knowledge (description) VALUES('Desarrollador Front End');
INSERT INTO knowledge (description) VALUES('Desarrollador Back End');
INSERT INTO knowledge (description) VALUES('Analista de datos');
INSERT INTO knowledge (description) VALUES('Lider de Proyecto');
INSERT INTO knowledge (description) VALUES('Diseñador Web/UI/UX');
INSERT INTO knowledge (description) VALUES('Analisis Funcional');
INSERT INTO knowledge (description) VALUES('Analista de Calidad/Tester/QA');
INSERT INTO knowledge (description) VALUES('Administrador de Base de datos');
INSERT INTO knowledge (description) VALUES('Administrador de Redes');
INSERT INTO knowledge (description) VALUES('Implementador de Sistemas');
INSERT INTO knowledge (description) VALUES('Comunicación Digital');

TRUNCATE TABLE empleo RESTART IDENTITY CASCADE;
INSERT INTO empleo (description) VALUES('Sector Público Nacional');
INSERT INTO empleo (description) VALUES('Sector Público Provincial');
INSERT INTO empleo (description) VALUES('Sector Público Internacional');
INSERT INTO empleo (description) VALUES('Sector Privado de IT');
INSERT INTO empleo (description) VALUES('Sector Privado Financiero');
INSERT INTO empleo (description) VALUES('Sector Privado Industrial');
INSERT INTO empleo (description) VALUES('Sector Privado Agroindustrial');
INSERT INTO empleo (description) VALUES('Sector Privado Servicios(Comercio/Turismo/Profesionales)');
INSERT INTO empleo (description) VALUES('Beca. Tutoria. Pasantía. Programa Laboral. Plan Social.');
INSERT INTO empleo (description) VALUES('Emprendedor');
INSERT INTO empleo (description) VALUES('Desarrollador Independiente');
INSERT INTO empleo (description) VALUES('Sin Empleo');

TRUNCATE TABLE motivation RESTART IDENTITY CASCADE;
INSERT INTO motivation (description) VALUES('Acceder a un certificado que acredite mis conocimientos');
INSERT INTO motivation (description) VALUES('Conseguir trabajo relacioando con IT');
INSERT INTO motivation (description) VALUES('Progresar en mi trabajo actual');

TRUNCATE TABLE courses RESTART IDENTITY CASCADE ;
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Argentina Programa', 'Argentina Programa', false, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('111 Programadores', '111 Programadores', false, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Mern Stack UNLaR 1era Edición', 'Mern Stack UNLaR 1era Edición', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Metodologías Ágiles (Gobierno de La Rioja/MateriaD)', 'Metodologías Ágiles (Gobierno de La Rioja/MateriaD)', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Diseño UX/UI (Gobierno de La Rioja/MateriaD)', 'Diseño UX/UI (Gobierno de La Rioja/MateriaD)', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Cursos Front End o Back End (Gobierno de La Rioja/MateriD)', 'Cursos Front End o Back End (Gobierno de La Rioja/MateriD)', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Data Analytics (Gobierno de La Rioja/MateriaD)', 'Data Analytics (Gobierno de La Rioja/MateriaD)', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Introducción a la Programación (Gobierno de La Rioja/MateriaD)', 'Introducción a la Programación (Gobierno de La Rioja/MateriaD)', true, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Cursos gratuitos on line', 'Cursos gratuitos on line', false, false);
INSERT INTO courses (name, description, is_from_gob_larioja, is_open) VALUES('Cursos pagos on line', 'Cursos pagos on line', false, false);
