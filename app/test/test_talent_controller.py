"""Unit tests for api.LlankayTalent_... API endpoint."""
from http import HTTPStatus
import unittest
from datetime import datetime
from run import app, db
from app.test.util import administrator_user, register_user
from . import BaseTestClass
from flask import url_for
from app.main.model.genre import Genre
from app.main.model.education_level import EducationLevel
from app.main.model.larioja_depto import LaRiojaDepto
from app.main.model.talent import Talent


class TestTalentController(BaseTestClass):

    def test_talent_register(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.LlankayTalent_talent_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "2",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Talent Successfully registered.")

    def test_talent_already_registered(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.LlankayTalent_talent_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "2",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Talent Successfully registered.")

            response = c.post(
                url_for("api.LlankayTalent_talent_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "2",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CONFLICT, response.status_code)
            self.assertEqual(response.json["message"], "Talent already exists. With eamanu@yaerobi.com.")

    def test_talent_updated(self):
        token = administrator_user()

        with app.test_client() as c:
            response = c.post(
                url_for("api.LlankayTalent_talent_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "2",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Talent Successfully registered.")

            response = c.put(
                url_for("api.LlankayTalent_talent_update"),

                json={"name": "Gabriel",
                      "surname": "Jesus",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "312",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
            self.assertEqual(response.json["message"], "Talent Successfully updated.")

    def test_talent_get_controller(self):
        token = administrator_user()
        with app.test_client() as c:
            response = c.post(
                url_for("api.LlankayTalent_talent_new"),

                json={"name": "Emmanuel",
                      "surname": "Arias",
                      "email": "eamanu@yaerobi.com",
                      "dni": "2",
                      "cellphone": "2",
                      "linkedin": "linkedin2",
                      "other_webpage": "other_webpage2",
                      "bio": "bio2",
                      "github": "github2",
                      "gitlab": "gitlab2",
                      "birthday": datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat(),
                      "address": "Fatima calle 2",
                      "residence": None,
                      "genre": None,
                      "education": [{"name": "UNLAR0", "year_graduation": 2010, "titulo": "INGENIERO"},
                                    {"name": "UNLAR1", "year_graduation": 2011, "titulo": "Contador"}],
                      "education_level": None,
                      "empleo": [{"description": "vendedor0"}, {"description": "vendedor1"}]},

                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.CREATED, response.status_code)
            self.assertEqual(response.json["message"], "Talent Successfully registered.")

        with app.test_client() as c:
            response = c.get(
                url_for("api.LlankayTalent_talent_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, response.status_code)

    def test_get_all_genre_controller(self):
        token = administrator_user()
        genre = [Genre(name=f"identity{i}") for i in range(3)]
        [db.session.add(n) for n in genre]
        db.session.commit()

        with app.test_client() as c:
            all_genre = c.get(
                url_for("api.LlankayTalent_genre_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.OK, all_genre.status_code)
            self.assertEqual(3, len(all_genre.json))
            [self.assertEqual(f"identity{i}", genre["name"])
             for i, genre in enumerate(all_genre.json) if i < 3]

    def test_get_all_educationlvl_controller(self):
        token = administrator_user()
        educlevel = [EducationLevel(name=f"level{i}") for i in range(3)]
        [db.session.add(n) for n in educlevel]
        db.session.commit()

        with app.test_client() as c:
            all_educationlvl = c.get(
                url_for("api.LlankayTalent_education_level_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.OK, all_educationlvl.status_code)
            self.assertEqual(3, len(all_educationlvl.json))
            [self.assertEqual(f"level{i}", education["name"])
             for i, education in enumerate(all_educationlvl.json) if i < 3]

    def test_get_all_residence_controller(self):
        token = administrator_user()
        residence = [LaRiojaDepto(name=f"place{i}") for i in range(3)]
        [db.session.add(n) for n in residence]
        db.session.commit()

        with app.test_client() as c:
            all_residence = c.get(
                url_for("api.LlankayTalent_residence_get"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
            )
            self.assertEqual(HTTPStatus.OK, all_residence.status_code)
            self.assertEqual(3, len(all_residence.json))
            [self.assertEqual(f"place{i}", residence["name"])
             for i, residence in enumerate(all_residence.json) if i < 3]

    def test_talent_get_all_controller(self):
        [register_user(f"eamanu{i}@yaerobi", "hola") for i in range(3)]
        talents = [Talent(name="Emmanuel",
                          surname="Arias",
                          email=f"eamanu{i}@yaerobi",
                          dni=i,
                          cellphone="2",
                          linkedin="linkedin2",
                          other_webpage="other_webpage2",
                          bio="bio2",
                          github="github2",
                          gitlab="gitlab2",
                          birthday=datetime(2019,5,5),
                          address="Fatima calle 2",
                                   ) for i in range(3)]
        [db.session.add(n) for n in talents]
        db.session.commit()
        token = administrator_user()
        with app.test_client() as c:
            all_talent = c.get(
                url_for("api.LlankayTalent_talent_list"),
                content_type="application/json",
                headers={"Authorization": f"Bearer {token.json['access_token']}"},
                )
            self.assertEqual(HTTPStatus.OK, all_talent.status_code)
            self.assertEqual(3, len(all_talent.json))
            [self.assertEqual(f"eamanu{i}@yaerobi", talent["email"])
             for i, talent in enumerate(all_talent.json) if i < 3]


if __name__ == "__main__":
    unittest.main()
