from app.main import db


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('Users', backref='role', lazy='dynamic')
