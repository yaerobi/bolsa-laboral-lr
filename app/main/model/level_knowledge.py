from app.main import db


class LevelKnowledge(db.Model):
    __tablename__ = "level_knowledge"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(20), nullable=False)
