from app.main import db


class Motivation(db.Model):
    __tablename__ = "motivation"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(255), nullable=False)
