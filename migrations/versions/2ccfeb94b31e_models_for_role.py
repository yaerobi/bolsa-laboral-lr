"""models_for_role

Revision ID: 2ccfeb94b31e
Revises: 5742da6ac4ec
Create Date: 2022-04-15 20:46:57.662525

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column
import datetime


# revision identifiers, used by Alembic.
revision = '2ccfeb94b31e'
down_revision = '5742da6ac4ec'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('roles',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=64), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.add_column('users', sa.Column('role_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'users', 'roles', ['role_id'], ['id'])
    # Insert roles
    roles = table('roles', column('name'))
    op.bulk_insert(roles, [
        {"name": "admin"},
        {"name": "talent"},
        {"name": "business"},
        {"name": "business_responsable"},
        {"name": "supervisor"},
        {"name": "viewer"},
    ])
    # insert user admin
    users = table('users',
                  column('email'),
                  column('password_hash'),
                  column('role_id'),
                  column("register_on")
                  )
    op.bulk_insert(users, [
        {"email": "admin@admin",
         "password_hash": "$2a$04$PZLNSaKrs3XbfVNzfLJ5iOhVKIQohoUUWUKPUrL4f0tD7b.ZIZ0w.",
         "role_id": 1,
         "register_on": datetime.datetime.utcnow()}
    ])

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'users', type_='foreignkey')
    op.drop_column('users', 'role_id')
    op.drop_constraint(None, 'talent', type_='foreignkey')
    op.create_foreign_key('talent_user_id_fkey', 'talent', 'users', ['user_id'], ['id'])
    op.drop_table('roles')
    # ### end Alembic commands ###
