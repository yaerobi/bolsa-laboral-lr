import datetime
import os
from http import HTTPStatus

from flask import jsonify

from app.main import db
from app.main.model.user import Users
from app.main.utils.db_utils import get_all, save_changes
from app.main.service.verification_email import send_validation_mail
from flask import current_app, send_from_directory

from werkzeug.utils import secure_filename
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg"}
UPLOAD_FOLDER = os.path.abspath("./uploads/")


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1] in ALLOWED_EXTENSIONS


def save_new_user(data, role=2):
    user = Users.query.filter_by(email=data["email"]).first()
    if not user:
        new_user = Users(
            password=data["password"],
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email=data["email"],
            role_id=role
            # country_city_id=data["country_city_id"],
        )
        # TODO: Fix this in the future
        # save_technologies(new_user, data['technologies'])
        save_changes(new_user)
        response = jsonify(
            status="success",
            message="successfully registered",
        )
        response.status_code = HTTPStatus.CREATED
        msg, _ = send_validation_mail(new_user.email)
        print(msg)
        return response
    else:
        response = jsonify(
            status="fail",
            message=f"Email {data['email']}, is already registered",
        )
        response.status_code = HTTPStatus.CONFLICT
        return response


def edit_role_user_administrator(data):
    user = Users.query.filter_by(email=data["email"]).first()
    user.role_id = data["role_id"]
    db.session.commit()
    response = jsonify(
        status="success",
        message="User Updated.",
    )
    response.status_code = HTTPStatus.ACCEPTED
    return response


# def save_technologies(user: Users, technologies_list: List):
    # for tech in technologies_list:
        # technology = Technologies.query.filter_by(id=tech)
        # user.technologies.append(technology)


def get_all_users():
    return get_all(Users)


def get_a_user(email: str):
    return Users.query.filter_by(email=email).first()


def upload_files(f, email):
    if f.filename == "":
        response = jsonify(
            status="fail",
            message="No file selected.",
        )
        return response
    if f and allowed_file(f.filename):
        user = Users.query.filter_by(email=email).first()
        f_name = user.email.split("@")[0] + str(hash(f.filename))
        filename = secure_filename(f_name)
        f.save(os.path.join(current_app.config["UPLOAD_FOLDER"], filename))

        user.photo = filename
        db.session.commit()
        response = jsonify(
            status="success",
            message=f"File successfully save.",
        )
        response.status_code = HTTPStatus.CREATED
        return response


def get_file(email: str):
    user = Users.query.filter_by(email=email).first()
    filename = user.photo
    if filename is None or filename == "":
        filename = "default.jpg"
    return send_from_directory(current_app.config['UPLOAD_FOLDER'],
                               filename, as_attachment=True)


def is_profile_user_complete(email: str) -> bool:
    user = Users.query.filter_by(email=email).first()
    return bool(user.is_profile_complete)
