"""Extensions registry
All extensions here are used as singletons and
initialized in application factory
"""

from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy


jwt = JWTManager()
db = SQLAlchemy()
flask_bcrypt = Bcrypt()
mail = Mail()
