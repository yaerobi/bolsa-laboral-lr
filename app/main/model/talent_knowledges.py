from app.main import db


class TalentKnowledge(db.Model):
    __tablename__ = "talent_knowledge"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    talent_id = db.Column(db.Integer, db.ForeignKey("talent.id"), nullable=True)
    knowledge_id = db.Column(db.Integer, db.ForeignKey("knowledge.id"), nullable=True)
    level_knowledge_id = db.Column(
        db.Integer, db.ForeignKey("level_knowledge.id"), nullable=True
    )

    def __init__(self, talent_id: int, knowledge_id: int, level_knowledge_id: int):
        self.talent_id = talent_id
        self.knowledge_id = knowledge_id
        self.level_knowledge_id = level_knowledge_id
