from app.main import db


def get_all(data):
    return data.query.all()


def save_changes(data):
    try:
        db.session.add(data)
        db.session.commit()
    except Exception as err:
        # TODO: Change to log system
        print(err)
        return False
    return True
