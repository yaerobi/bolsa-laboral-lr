import unittest

from flask_testing import TestCase
from run import app, db


class BaseTestClass(TestCase):
    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def create_app(self):
        app.config.from_object("app.main.config.TestingConfig")
        return app