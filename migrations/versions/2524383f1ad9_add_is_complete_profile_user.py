"""add is complete profile user

Revision ID: 2524383f1ad9
Revises: 5948b678026b
Create Date: 2022-07-25 01:28:01.012052

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2524383f1ad9'
down_revision = '5948b678026b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('is_profile_complete', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('users', 'is_profile_complete')
    # ### end Alembic commands ###
