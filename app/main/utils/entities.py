from enum import IntEnum


class Roles(IntEnum):
    ADMIN = 1
    TALENT = 2
    BUSINESS = 3
    BUSINESS_RESPONSABLE = 4
    SUPERVISOR = 5
    VIEWER = 6

    def __str__(self):
        return _ROLES_TRANSLATOR[self]


_ROLES_TRANSLATOR = {
    Roles.ADMIN: "admin",
    Roles.TALENT: "talent",
    Roles.BUSINESS: "business",
    Roles.BUSINESS_RESPONSABLE: "business_responsable",
    Roles.SUPERVISOR: "supervisor",
    Roles.VIEWER: "viewer",
}
