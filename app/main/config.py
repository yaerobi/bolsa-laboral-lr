import os
from datetime import timedelta

# uncomment the line below for postgres database url from environment variable
# export DATABASE_URL=postgresql://<username>:<password>@<server>:5432/<db_name>
postgres_local_base = os.environ.get("DATABASE_URL")

basedir = os.path.abspath(os.path.dirname(__file__))
UPLOAD_FOLDER = os.path.abspath("./uploads/")


class Config:
    SECRET_KEY = os.getenv("SECRET_KEY", "I hate Windchot")  # FIXME: Change this!
    DEBUG = False
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY", "I hate Windchot")  # FIXME: Change this!
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=24)

    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_FROM = os.getenv("MAIL_FROM")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
    MAIL_PORT = os.getenv("MAIL_PORT", 123)
    MAIL_SERVER = os.getenv("MAIL_SERVER")
    MAIL_USE_SSL = os.getenv("MAIL_USE_SSL", True)
    MAIL_DEBUG = False

    SEND_MAIL = False

    UPLOAD_FOLDER = UPLOAD_FOLDER


class DevelopmentConfig(Config):
    # uncomment the line below to use postgres
    SQLALCHEMY_DATABASE_URI = postgres_local_base
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SEND_MAIL = True


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "itjobs_lr_test.db")
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    MAIL_LLANKAY_VALIDATION = False
    MAIL_LLANKAY_PASSWORD = False


class ProductionConfig(Config):
    DEBUG = False
    # uncomment the line below to use postgres
    SQLALCHEMY_DATABASE_URI = postgres_local_base
    SEND_MAIL = True


config_by_name = dict(dev=DevelopmentConfig, test=TestingConfig, prod=ProductionConfig)

key = Config.SECRET_KEY
