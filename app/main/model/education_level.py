from app.main import db


class EducationLevel(db.Model):
    __tablename__ = "education_level"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
