import datetime
from http import HTTPStatus

from flask import request, jsonify
from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity, jwt_required, get_jwt
from flask_restx import Resource

from app.main import flask_bcrypt
from app.main.service.auth_helper import Auth, Logout, send_token_reset_password, change_password
from app.main.service.verification_email import validate_email
from app.main.utils.dto import AuthDto
from app.main.utils.entities import Roles

BEFORE_EXP = 30

api = AuthDto.api
user_auth = AuthDto.user_auth


@api.route("/login")
class UserLogin(Resource):
    """Handles HTTP requests to URL: /llankay/v1/auth/login."""

    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("user login")
    @api.expect(user_auth, validate=True)
    def post(self):
        """User Login Resource"""
        post_data = request.json
        role, code = Auth.login_user(data=post_data)
        if code == 200 and role == Roles.TALENT:
            access_token = create_access_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            refresh_token = create_refresh_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            # TODO: jsonify seems does not work with flask_restx
            # analize the best way to return the "access_token"
            response = jsonify(access_token=access_token, refresh_token=refresh_token)
            response.status_code = HTTPStatus.ACCEPTED
            return response
        elif code == 401:
            response = jsonify(
                status="fail",
                message=f"Bad username or password.",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response
        else:
            response = jsonify(
                status="fail",
                message=f"Hey, don't try to hack me ;).",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response


@api.route("/business/login")
class UserLogin(Resource):
    """Handles HTTP requests to URL: /llankay/v1/auth/businesslogin."""

    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("Business login")
    @api.expect(user_auth, validate=True)
    def post(self):
        """Business Login Resource"""
        post_data = request.json
        role, code = Auth.login_user(data=post_data)
        if code == 200 and role == Roles.BUSINESS_RESPONSABLE:
            access_token = create_access_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            refresh_token = create_refresh_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            # TODO: jsonify seems does not work with flask_restx
            # analize the best way to return the "access_token"
            response = jsonify(access_token=access_token, refresh_token=refresh_token)
            response.status_code = HTTPStatus.ACCEPTED
            return response
        elif code == 401:
            response = jsonify(
                status="fail",
                message=f"Bad username or password.",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response
        else:
            response = jsonify(
                status="fail",
                message=f"Hey, don't try to hack me ;).",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response


@api.route("/admin/login")
class UserLogin(Resource):
    """Handles HTTP requests to URL: /llankay/v1/auth/businesslogin."""

    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("Admin login")
    @api.expect(user_auth, validate=True)
    def post(self):
        """Admin Login Resource"""
        post_data = request.json
        role, code = Auth.login_user(data=post_data)
        if code == 200 and role == Roles.ADMIN:
            access_token = create_access_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            refresh_token = create_refresh_token(
                identity=post_data.get("email"),
                additional_claims={"role": str(role)})
            # TODO: jsonify seems does not work with flask_restx
            # analize the best way to return the "access_token"
            response = jsonify(access_token=access_token, refresh_token=refresh_token)
            response.status_code = HTTPStatus.ACCEPTED
            return response
        elif code == 401:
            response = jsonify(
                status="fail",
                message=f"Bad username or password.",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response
        else:
            response = jsonify(
                status="fail",
                message=f"Hey, don't try to hack me ;).",
            )
            response.status_code = HTTPStatus.CONFLICT
            return response


@api.route("/logout/access")
class LogoutAccess(Resource):
    """Handles HTTP requests to URL: /llankay/v1/auth/logout/access."""

    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("logout a user with access token revoked")
    @jwt_required()
    def post(self):
        """Logout Resource"""
        jti = get_jwt()['jti']
        code = Logout.save_revoked_token(jti=jti)
        if code == 200:
            response = jsonify(
                status="success",
                message=f"access token has been revoked.",
            )
            response.status_code = HTTPStatus.OK
            return response

    
@api.route("/logout/refresh")
class LogoutRefresh(Resource):
    """Handles HTTP requests to URL: /llankay/v1/auth/logout/refresh."""

    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc("logout a user with refresh token revoked")
    @jwt_required(refresh=True)
    def post(self):
        """ User Logout Refresh Api """
        jti = get_jwt()['jti']
        code = Logout.save_revoked_token(jti=jti)
        if code == 200:
            response = jsonify(
                status="success",
                message=f"refresh token has been revoked.",
            )
            response.status_code = HTTPStatus.OK
            return response


@api.route("/token/refresh")    
class TokenRefresh(Resource):

    @api.doc("When the access token is revoked, you should generate a new access token with this service")
    @jwt_required(refresh=True)
    def post(self):
        """ When the access token is revoked, you should generate a new access token with this service
        and with the refresh_token"""
        try:       
            exp_timestamp = get_jwt()["exp"]
            now = datetime.datetime.now(datetime.timezone.utc)
            target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(minutes=BEFORE_EXP))
            if target_timestamp > exp_timestamp:
                access_token = create_access_token(identity=get_jwt_identity(),
                                                   additional_claims={"role": get_jwt()['role']})
                response = jsonify(access_token=access_token,
                                   status="success",
                                   message=f"access token successfully created.",)
                response.status_code = HTTPStatus.ACCEPTED
                return response
        except (RuntimeError, KeyError):
            # Case where there is not a valid JWT. Just return the original response
            return {'status': 'fail', 'message': 'RuntimeError'}, 500


@api.route("/validate-email/<token>")
class ValidateEmail(Resource):

    @api.doc("Validate mail")
    def get(self, token):
        """Validate mail"""
        msg, status_code = validate_email(token)
        return {"msg": msg}, status_code


@api.route("/reset-password/<user>")
class GenerateNewPassword(Resource):
    @api.doc("Generate token to reset password")
    def get(self, user):
        """Generate token to reset password"""
        msg, status = send_token_reset_password(user)
        return {"msg": msg}, status


@api.route("/new-password")
class ResetPassword(Resource):
    @api.doc("Change password")
    def post(self):
        post_data = request.json
        msg, status = change_password(post_data.get("token"), post_data.get("password"))
        return {"msg": msg}, status
