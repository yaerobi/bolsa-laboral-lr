from app.main import db

SIMPLE_DATA_COLUMNS = ("name", "description", "email", "phone", "webpage", "address")


class Empresa(db.Model):
    __tablename__ = "empresa"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    phone = db.Column(db.String(20), nullable=True)
    linkedin = db.Column(db.String(255), nullable=True)
    webpage = db.Column(db.String(255), nullable=True)
    address = db.Column(db.String, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)

    def simple_data(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name in SIMPLE_DATA_COLUMNS}
