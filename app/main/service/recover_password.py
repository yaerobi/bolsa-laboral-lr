import datetime
from typing import Tuple

import jinja2
from flask_mail import Message
from jose import jwt
from jose.exceptions import JWTError

from app.main import db
from app.main.configurations import (
    SECRET_KEY,
    ALGORITHM,
    RECOVER_PASSWORD_URL,
    DEBUG_MAIL_VALIDATION,
    MAIL_FROM,
)
from app.main.extensions import mail
from app.main.model.user import Users
from app.main.model.talent import Talent

JWT_AUD = "validation-mail"


MAIL_TEMPLATE = """
<!DOCTYPE html>
<head></head>
<body>
<p>Hola {{ name }} {{ surname }},</p>
</p>
<p>Gracias por registrarte en el portal Llankay, para validar tu email haz clic en el siguiente enlace {{ url }}.</p>
</body>
"""


class ValidationError(Exception):
    """Some Error during the validation mail"""


def send_recovery_password(email: str) -> Tuple[str, int]:
    user = Users.query.filter_by(email=email).first()
    if not user:
        # TODO: Logger
        msg = "User doesn't exists"
        print(msg)
        return "If your exists you will receive a email", 200
    if not user.validated_mail:
        msg = "User didn't validate the email"
        print(msg)
        return "If your email exists you will receive a email", 200
    talent = Talent.query.filter(Talent.user_id == user.id).first()

    validation_url = generate_recovery_url(user)
    recipients = DEBUG_MAIL_VALIDATION if DEBUG_MAIL_VALIDATION else user.email  # TODO: REMOVE THIS BEFORE GO TO PRODUCTION

    _send_email(
        talent.name,
        talent.surname,
        recipients,
        validation_url,
    )
    # TODO: Cambiar a log
    msg = "Email sent successfully."
    print(msg)
    return "If your exists you will receive a email", 200


def generate_recovery_url(user: Users) -> str:
    now = datetime.datetime.now()
    claims = {
        "exp": now + datetime.timedelta(hours=24),
        "iss": user.email,
        "iat": now,
        "aud": JWT_AUD,
    }
    token = jwt.encode(claims, SECRET_KEY, algorithm=ALGORITHM)
    url = f"{RECOVER_PASSWORD_URL}/{token}"
    # TODO: usar log
    print(f"Created url={url}")
    return url


def _send_email(name: str, surname: str, recipients: str, validation_link: str):
    msg = Message("Recuperación de contraseña - Llankay", sender=MAIL_FROM,
                  recipients=[recipients])
    values = {"name": name, "surname": surname, "url": validation_link}
    msg.html = jinja2.Template(MAIL_TEMPLATE).render(values)

    mail.send(msg)


def recovery_password(token: str, password: str):
    to_verify = {
        'verify_signature': True,
        'verify_aud': True,
        'verify_exp': True,
        'require_aud': True,
        'require_iat': True,
        'require_exp': True,
        'require_iss': True,
    }
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM], options=to_verify, audience=JWT_AUD)
        mail = payload["iss"]
        user = Users.query.filter(Users.email == mail).first()
        if user is None:
            raise ValidationError(f"User doesn't exist")
        if not bool(user.validated_mail):
            raise ValidationError(f"Something wrong, password cannot be change.")
        user.password = password
        db.session.commit()

    except (KeyError, ValidationError) as err:
        # TODO: cambiar a log
        print(f"Error: {str(err)}")
        message = "Something wrong, password cannot be changed."
        return message, 400
    except JWTError as err:
        # TODO: cambiar a log
        print(f"Something wrong with the token decode: {str(err)}")
        message = "Something wrong, password cannot be changed."
        return message, 400
    return "User password changed successfully", 200
