import unittest
import datetime

from http import HTTPStatus

from run import db
from app.main.service.user_service import (
    get_all_users,
)
from app.main.service.empresa_service import (
    save_new_empresa,
    edit_empresa,
    get_all_empresas,
    get_a_empresa,
)
from app.main.model.user import Users
from app.main.model.empresa import Empresa
from . import BaseTestClass


class TestEmpresaServices(BaseTestClass):

    def test_save_new_empresa(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["description"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["phone"] = "2"
        data["webpage"] = "webpage2"
        data["address"] = "Fatima calle 2"

        response = save_new_empresa(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["message"], "Empresa Successfully registered.")

        empresa_result = Empresa.query.filter_by(email="eamanu@yaerobi.com").first()

        self.assertEqual("eamanu@yaerobi.com", empresa_result.email)

    def test_get_all_empresas(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="ricardo@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )
        empresa1 = Empresa(
            user_id=1,
            name="Ricardo",
            description="Andino",
            email="ricardo@yaerobi",
            phone="1",
            webpage="other_webpage",
            address="Fatima calle 1"
        )

        empresa2 = Empresa(
            user_id=2,
            name="Emmanuel",
            description="Arias",
            email="eamanu2@yaerobi",
            phone="2",
            webpage="other_webpage2",
            address="Fatima calle 2"

        )

        db.session.add(user1)
        db.session.add(user2)

        db.session.add(empresa1)
        db.session.add(empresa2)

        db.session.commit()

        all_empresas = get_all_empresas()

        self.assertEqual(2, len(all_empresas))
        for empresa in all_empresas:
            user = Users.query.filter_by(email=empresa.email).first()
            self.assertEqual(empresa.email, user.email)

    def test_get_a_empresa(self):
        self.assertEqual([], get_all_users())

        empresa1 = Empresa(
            user_id=1,
            name="Ricardo",
            description="Andino",
            email="ricardo@yaerobi",
            phone="1",
            webpage="other_webpage",
            address="Fatima calle 1"
        )

        empresa2 = Empresa(
            user_id=2,
            name="Emmanuel",
            description="Arias",
            email="eamanu2@yaerobi",
            phone="2",
            webpage="other_webpage2",
            address="Fatima calle 2"

        )

        db.session.add(empresa1)
        db.session.add(empresa2)

        db.session.commit()

        empresa = get_a_empresa("eamanu2@yaerobi")

        self.assertEqual("Emmanuel", empresa.name)
        self.assertEqual("Arias", empresa.description)
        self.assertEqual("eamanu2@yaerobi", empresa.email)

        empresa = get_a_empresa("ricardo@yaerobi")

        self.assertEqual("Ricardo", empresa.name)
        self.assertEqual("Andino", empresa.description)
        self.assertEqual("ricardo@yaerobi", empresa.email)

    def test_response_empresa_update(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["description"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["phone"] = "2"
        data["webpage"] = "webpage2"
        data["address"] = "Fatima calle 2"

        response = save_new_empresa(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["message"], "Empresa Successfully registered.")

        data = dict()
        data["name"] = "Emmanuel"
        data["description"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["phone"] = "5"
        data["webpage"] = "sabina.com"
        data["address"] = "Fatima calle 8"

        response = edit_empresa(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
        self.assertEqual(response.json["message"], "Empresa Successfully updated.")


if __name__ == "__main__":
    unittest.main()
