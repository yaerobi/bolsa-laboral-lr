import datetime
from typing import Tuple

import jinja2
from flask_mail import Message
from jose import jwt
from jose.exceptions import JWTError

from app.main import db
from app.main.configurations import (
    SECRET_KEY,
    ALGORITHM,
    VALIDATION_MAIL_URL,
    DEBUG_MAIL_VALIDATION,
    MAIL_FROM,
)
from app.main.extensions import mail
from app.main.model.user import Users

from flask import current_app

JWT_AUD = "validation-mail"


MAIL_TEMPLATE = """
<!DOCTYPE html>
<head></head>
<body>
<p>Hola,</p>
</p>
<p>Gracias por registrarte en el portal Llankay, para validar tu email haz clic en el siguiente enlace {{ url }}.</p>
</body>
"""


class ValidationError(Exception):
    """Some Error during the validation mail"""


def send_validation_mail(email: str) -> Tuple[str, int]:
    user = Users.query.filter_by(email=email).first()
    if not user:
        # TODO: Logger
        msg = "No user to validate email"
        print(msg)
        return msg, 400
    if user.validated_mail:
        msg = "User already validate mail"
        print(msg)
        return msg, 400

    validation_url = generate_validation_url(user)
    recipients = DEBUG_MAIL_VALIDATION if DEBUG_MAIL_VALIDATION else user.email  # TODO: REMOVE THIS BEFORE GO TO PRODUCTION

    with current_app.app_context():
        send_mail = current_app.config["SEND_MAIL"]

    if send_mail:
        _send_email(
            recipients,
            validation_url,
        )
    # TODO: Cambiar a log
    msg = "Email sent successfully."
    print(msg)

    return msg, True


def generate_validation_url(user: Users) -> str:
    now = datetime.datetime.now()
    claims = {
        "exp": now + datetime.timedelta(hours=24),
        "iss": user.email,
        "iat": now,
        "aud": JWT_AUD,
    }
    token = jwt.encode(claims, SECRET_KEY, algorithm=ALGORITHM)
    url = f"{VALIDATION_MAIL_URL}/{token}"
    # TODO: usar log
    print(f"Created url={url}")
    return url


def _send_email(recipients: str, validation_link: str):
    msg = Message("Validación de Email - Llankay", sender=MAIL_FROM,
                  recipients=[recipients])
    values = {"url": validation_link}
    msg.html = jinja2.Template(MAIL_TEMPLATE).render(values)

    mail.send(msg)


def validate_email(token: str):
    to_verify = {
        'verify_signature': True,
        'verify_aud': True,
        'verify_exp': True,
        'require_aud': True,
        'require_iat': True,
        'require_exp': True,
        'require_iss': True,
    }
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM], options=to_verify, audience=JWT_AUD)
        mail = payload["iss"]
        user = Users.query.filter(mail == Users.email).first()
        if user is None:
            raise ValidationError(f"User doesn't exist")
        # Validate user and save
        if bool(user.validated_mail):
            raise ValidationError(f"User already email_validated")
        user.validated_mail = 1
        db.session.commit()

    except (KeyError, ValidationError) as err:
        # TODO: cambiar a log
        print(f"Error: {str(err)}")
        message = "Something wrong, mail cannot be email_validated."
        return message, 400
    except JWTError as err:
        # TODO: cambiar a log
        print(f"Something wrong with the token decode: {str(err)}")
        message = "Something wrong, mail cannot be email_validated."
        return message, 400
    return "User email_validated successfully", 200
