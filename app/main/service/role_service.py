from app.main.model.user_rol import Role
from app.main.utils.db_utils import get_all, save_changes
from http import HTTPStatus
from flask import jsonify


def save_new_role(data):
    role = Role.query.filter_by(name=data["name"]).first()
    if not role:
        new_role = Role(
            name=data["name"]
        )
        save_changes(new_role)
        response = jsonify(
            status="success",
            message="Role successfully registered",
        )
        response.status_code = HTTPStatus.CREATED
        return response
    else:
        response = jsonify(
            status="fail",
            message=f"Role {data['name']}, is already registered",
        )
        response.status_code = HTTPStatus.CONFLICT
        return response


def get_all_roles():
    return get_all(Role)
