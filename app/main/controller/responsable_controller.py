from flask import request
from flask_restx import Resource, fields
from flask_jwt_extended import jwt_required, get_jwt_identity

from app.main.service.responsable_service import (save_new_responsable,
                                             get_a_responsable,
                                             edit_responsable,
                                             get_all_responsable)
from app.main.utils.decorators.role import admin_required
from app.main.utils.dtos.responsable_dto import ResponsableDto
from http import HTTPStatus


api = ResponsableDto.api
_llankay_responsable = ResponsableDto.responsable
_llankay_responsableGet = ResponsableDto.responsableGet


@api.route("/create_new")
class ResponsableNew(Resource):
    """Handles HTTP requests to URL: /llankay/v1/responsable/create_new."""

    @api.response(int(HTTPStatus.CREATED), "Responsable Successfully registered.")
    @api.response(int(HTTPStatus.CONFLICT), "Responsable already exists.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.doc("Responsable registration")
    @api.expect(_llankay_responsable, validate=False)
    @jwt_required()
    def post(self):
        """Create a new Responsable"""
        responsable = request.json
        user = get_jwt_identity()
        return save_new_responsable(responsable, user)


@api.route("/get_responsable")
class ResponsableGet(Resource):
    """Handles HTTP requests to URL: /llankay/v1/responsable/get_responsable."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.marshal_list_with(_llankay_responsableGet)
    @api.doc(security='Bearer')
    @jwt_required()
    def get(self):
        """Get self Responsable"""
        user = get_jwt_identity()
        return get_a_responsable(user)


@api.route("/edit_responsable")
class ResponsableUpdate(Resource):
    """Handles HTTP requests to URL: /llankay/v1/responsable/edit_responsable."""

    @api.response(int(HTTPStatus.ACCEPTED), "Responsable Successfully updated.")
    @api.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
    @api.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
    @api.doc(security='Bearer')
    @api.expect(_llankay_responsable, validate=False)
    @jwt_required()
    def put(self):
        """Edit Responsable"""
        responsable = request.json
        user = get_jwt_identity()
        return edit_responsable(responsable, user)


@api.route("/get_all")
class ResponsableList(Resource):
    """Handles HTTP requests to URL: /llankay/v1/responsable/get_all."""

    @api.response(int(HTTPStatus.OK), "Request Successfully done.")
    @api.doc(security='Bearer')
    @api.marshal_list_with(_llankay_responsableGet)
    @admin_required()
    def get(self):
        """List all registered users"""
        response = get_all_responsable()
        return response
