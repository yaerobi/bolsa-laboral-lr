from typing import Dict

from flask_restx import abort
from http import HTTPStatus
from flask import jsonify

from app.main import db
from app.main.model.empresa import Empresa
from app.main.model.user import Users
from app.main.utils.db_utils import save_changes, get_all


def save_new_empresa(data: Dict, user_id):
    empresa = Empresa.query.filter_by(email=user_id).first()
    user = Users.query.filter_by(email=user_id).first()
    if not empresa:
        new_empresa = Empresa(
            user_id=user.id,
            name=data["name"],
            description=data["description"],
            email=user_id,
            phone=data["phone"],
            webpage=data["webpage"],
            address=data["address"],
        )

        result_new_empresa = save_changes(new_empresa)

        if all([result_new_empresa]):
            response = jsonify(
                status="success",
                message="Empresa Successfully registered.",
            )
            response.status_code = HTTPStatus.CREATED
            return response
        return HTTPStatus.INTERNAL_SERVER_ERROR
    response = jsonify(
        status="fail",
        message=f"Empresa already exists. With user {data['email']}.",
    )
    response.status_code = HTTPStatus.CONFLICT
    return response


def edit_empresa(data, user_id):
    empresa = Empresa.query.filter_by(email=user_id).first()
    empresa.name = data["name"]
    empresa.description = data["description"]
    empresa.email = user_id
    empresa.phone = data["phone"]
    empresa.webpage = data["webpage"]
    empresa.address = data["address"]
    db.session.commit()
    response = jsonify(
        status="success",
        message="Empresa Successfully updated.",
    )
    response.status_code = HTTPStatus.ACCEPTED
    return response


def get_a_empresa(email: str):
    return (
        db.session.query(Empresa)
        .filter(Empresa.email == email)
        .first()
    )


def get_all_empresas():
    return get_all(Empresa)
