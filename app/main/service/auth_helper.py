import datetime
from typing import Tuple, Any

import jinja2
from flask import current_app
from flask_mail import Message
from jose.exceptions import JWTError

from app.main import db
from app.main import jwt
from jose import jwt as jwt_jose
from app.main.configurations import (
    SECRET_KEY,
    ALGORITHM,
    REGENERATION_PASSWORD_URL,
    DEBUG_MAIL_VALIDATION,
    MAIL_FROM,
)
from app.main.extensions import mail
from app.main.model.blocklist_token import RevokedTokenModel
from app.main.model.user import Users, Role
from app.main.utils.entities import Roles

JWT_AUD = "regenerate-password"

MAIL_TEMPLATE = """
<!DOCTYPE html>
<head></head>
<body>
<p>Hola,</p>
</p>
<p>Para regenerar el password por favor acceder a: {{ url }}.</p>
</body>
"""


class GenerationsPasswordException(Exception):
    """Some exceptions during the Generation of the new password"""


class Auth:
    @staticmethod
    def login_user(data) -> Tuple[Any, int]:
        try:
            # fetch the user data
            user = Users.query.filter_by(email=data.get("email")).first()
            if user and user.check_password(data.get("password")):
                role_data = Role.query.filter_by(id=user.role_id).first()
                if role_data is None:
                    role_data = Role()
                    role_data.id = 2
                role = Roles(role_data.id)
                return role, 200
            return None, 401
        except Exception as err:
            # TODO: log the error message
            print(err)
            return None, 500


class Logout:
    @staticmethod
    def save_revoked_token(jti):
        try:
            # Revoking access token
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return 200
        except Exception as err:
            return 500


# Checking that token is in blocklist or not
@jwt.token_in_blocklist_loader
def check_if_token_in_blocklist(header, decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blocklisted(jti)


def send_token_reset_password(email: str):
    user = Users.query.filter_by(email=email).first()
    if not user:
        # TODO: Logger
        msg = "No user to validate email"
        print(msg)
        return msg, 400
    if user.is_password_regenerated:
        msg = "User already generate a new password"
        print(msg)
        return msg, 400

    regenerate_password_url = generate_regeneration_password_url(user)
    recipients = DEBUG_MAIL_VALIDATION if DEBUG_MAIL_VALIDATION else user.email

    with current_app.app_context():
        send_mail = current_app.config["SEND_MAIL"]

    if send_mail:
        _send_email(
            recipients,
            regenerate_password_url,
        )
    # TODO: Cambiar a log
    msg = "Email sent successfully."
    print(msg)
    user.is_password_regenerated = True
    db.session.add(user)
    db.session.commit()
    return msg, True


def generate_regeneration_password_url(user: Users) -> str:
    now = datetime.datetime.now()
    claims = {
        "exp": now + datetime.timedelta(hours=24),
        "iss": user.email,
        "iat": now,
        "aud": JWT_AUD,
    }
    token = jwt_jose.encode(claims, SECRET_KEY, algorithm=ALGORITHM)
    url = f"{REGENERATION_PASSWORD_URL}/{token}"
    # TODO: usar log
    print(f"Created url={url}")
    return url


def _send_email(recipients: str, validation_link: str):
    msg = Message("Generar Password - Llankay", sender=MAIL_FROM,
                  recipients=[recipients])
    values = {"url": validation_link}
    msg.html = jinja2.Template(MAIL_TEMPLATE).render(values)

    mail.send(msg)


def change_password(token: str, password: str):
    to_verify = {
        'verify_signature': True,
        'verify_aud': True,
        'verify_exp': True,
        'require_aud': True,
        'require_iat': True,
        'require_exp': True,
        'require_iss': True,
    }
    try:
        payload = jwt_jose.decode(token, SECRET_KEY, algorithms=[ALGORITHM], options=to_verify, audience=JWT_AUD)
        mail = payload["iss"]
        user = Users.query.filter(mail == Users.email).first()
        if user is None:
            raise GenerationsPasswordException(f"User doesn't exist")
        # Validate user and save
        if not bool(user.is_password_regenerated):
            raise GenerationsPasswordException(f"Picaron, tratando de hackear? Mandame un mail eamanu@yaerobi.com")
        user.is_password_regenerated = 0
        user.password = password
        db.session.add(user)
        db.session.commit()

    except (KeyError, GenerationsPasswordException) as err:
        # TODO: cambiar a log
        print(f"Error: {str(err)}")
        message = "Picaron, tratando de hackear? Mandame un mail eamanu@yaerobi.com"
        return message, 400
    except JWTError as err:
        # TODO: cambiar a log
        print(f"Something wrong with the token decode: {str(err)}")
        message = "Something wrong, mail cannot be regenerate password."
        return message, 400
    return "User new password save successfully", 200
