from flask import jsonify
from flask_restx import Resource
from app.main.utils.dtos.public_dto import PublicDto
from app.main.model.provinces import Province

from http import HTTPStatus

api = PublicDto.api
_province = PublicDto.provinces


@api.route("/provinces")
class GetProvince(Resource):
    """Get list of provinces"""

    @api.response(int(HTTPStatus.OK), "Get provinces list")
    @api.doc("Get list of provinces registered in the system")
    def get(self):
        """Get list of provinces"""
        response = jsonify(data=[{province.id: province.name} for province in Province.query.all()])
        response.status_code = HTTPStatus.OK
        return response
