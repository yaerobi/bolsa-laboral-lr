import unittest
import datetime

from http import HTTPStatus

from run import db
from app.main.service.user_service import (
    get_all_users,
)
from app.main.service.responsable_service import (
    save_new_responsable,
    edit_responsable,
    get_all_responsable,
    get_a_responsable,
)
from app.main.model.user import Users
from app.main.model.responsable import Responsable
from . import BaseTestClass


class TestResponsableServices(BaseTestClass):

    def test_save_new_responsable(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["cellphone"] = "2"
        data["empresa_id"] = None
        response = save_new_responsable(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["message"], "Responsable Successfully registered.")

        responsable_result = Responsable.query.filter_by(email="eamanu@yaerobi.com").first()

        self.assertEqual("eamanu@yaerobi.com", responsable_result.email)

    def test_get_all_responsable(self):
        self.assertEqual([], get_all_users())

        user1 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="ricardo@yaerobi",
        )
        user2 = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu2@yaerobi",
        )
        responsable1 = Responsable(
            user_id=1,
            name="Ricardo",
            surname="Andino",
            email="ricardo@yaerobi",
            cellphone="1",
        )

        responsable2 = Responsable(
            user_id=2,
            name="Emmanuel",
            surname="Arias",
            email="eamanu2@yaerobi",
            cellphone="2",
        )

        db.session.add(user1)
        db.session.add(user2)

        db.session.add(responsable1)
        db.session.add(responsable2)

        db.session.commit()

        all_responsables = get_all_responsable()

        self.assertEqual(2, len(all_responsables))
        for responsable in all_responsables:
            user = Users.query.filter_by(email=responsable.email).first()
            self.assertEqual(responsable.email, user.email)

    def test_get_a_responsable(self):
        self.assertEqual([], get_all_users())

        responsable1 = Responsable(
            user_id=1,
            name="Ricardo",
            surname="Andino",
            email="ricardo@yaerobi",
            cellphone="1",
        )

        responsable2 = Responsable(
            user_id=2,
            name="Emmanuel",
            surname="Arias",
            email="eamanu2@yaerobi",
            cellphone="2",
        )

        db.session.add(responsable1)
        db.session.add(responsable2)

        db.session.commit()

        responsable = get_a_responsable("eamanu2@yaerobi")

        self.assertEqual("Emmanuel", responsable.name)
        self.assertEqual("Arias", responsable.surname)
        self.assertEqual("eamanu2@yaerobi", responsable.email)

        responsable = get_a_responsable("ricardo@yaerobi")

        self.assertEqual("Ricardo", responsable.name)
        self.assertEqual("Andino", responsable.surname)
        self.assertEqual("ricardo@yaerobi", responsable.email)

    def test_response_responsable_update(self):
        user = Users(
            password_hash="hola",
            register_on=datetime.datetime.utcnow(),
            last_pass_change=datetime.datetime.utcnow(),
            email="eamanu@yaerobi.com",
        )
        db.session.add(user)
        db.session.commit()

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["cellphone"] = "2"
        data["empresa_id"] = None

        response = save_new_responsable(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.CREATED, response.status_code)
        self.assertEqual(response.json["message"], "Responsable Successfully registered.")

        data = dict()
        data["name"] = "Emmanuel"
        data["surname"] = "Arias"
        data["email"] = "eamanu@yaerobi.com"
        data["cellphone"] = "5"
        data["empresa_id"] = None

        response = edit_responsable(data, "eamanu@yaerobi.com")
        self.assertEqual(HTTPStatus.ACCEPTED, response.status_code)
        self.assertEqual(response.json["message"], "Responsable Successfully updated.")


if __name__ == "__main__":
    unittest.main()
