from app.main import db


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        # Save Token in DB
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blocklisted(cls, jti):
        # Checking that token is blocklisted
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)
